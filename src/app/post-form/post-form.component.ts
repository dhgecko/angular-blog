import { PostService } from './../services/post.service';
import { Post } from './../post';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-post-form',
  templateUrl: './post-form.component.html',
  styleUrls: ['./post-form.component.scss']
})
export class PostFormComponent implements OnInit {
  postForm: FormGroup

  constructor(private formBuilder: FormBuilder, private postService: PostService,
              private router: Router) { }

  ngOnInit() {
    this.initForm()
  }

  initForm() {
    this.postForm = this.formBuilder.group({
      title: ['', Validators.required],
      content: ['', Validators.required]
    });
  }

  onSaveForm() {
    const title = this.postForm.get("title").value
    const content = this.postForm.get("content").value
    const newPost = new Post(title, content, new Date())
    this.postService.newPost(newPost)
    this.router.navigate(["/posts"])
  }
}

import { PostService } from './services/post.service';
import { Component } from '@angular/core';
import { Post } from './post';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  postsArray: Post[]

  constructor() {
    const config = {
      apiKey: "AIzaSyA5__eQO4JbtpPSzUcXe-YIaAVJ2TdjX6Q",
      authDomain: "tp-blog-cb8c2.firebaseapp.com",
      databaseURL: "https://tp-blog-cb8c2.firebaseio.com",
      projectId: "tp-blog-cb8c2",
      storageBucket: "tp-blog-cb8c2.appspot.com",
      messagingSenderId: "453106264929"
    };
    firebase.initializeApp(config);
  }

  ngOnInit() { }
}

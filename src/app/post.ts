export class Post {
    title: string
    content: string
    loveIts: number
    created_at: number
    
    constructor(title: string, content: string, created_at: Date) {
        this.title = title
        this.content = content
        this.created_at = created_at.getTime()
        this.loveIts = 0
    }
}
